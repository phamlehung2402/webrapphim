package com.project.movielogin;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.project.movielogin.helpers.Constant;
import com.project.movielogin.helpers.FileUploadUtil;
import com.project.movielogin.models.Movies;
import com.project.movielogin.services.MoviesService;

@Controller
public class AppController {

	@Autowired
	private MoviesService moviesService;
	
	@RequestMapping("/")
	public String viewHomePage(Model model) {
		return viewHomePage(model, 1, "name", "asc");
	}
	
	@RequestMapping("/page/{pageNum}")
	public String viewHomePage(Model model, @PathVariable(name = "pageNum") int pageNum,
				@Param("sortName") String sortName, @Param("sortDir") String direction) {
		
		if(sortName == null) {
			sortName = "name";
		}
		
		if(direction == null) {
			direction = "asc";
		}
		
		Page<Movies> page = moviesService.getMovesPage(pageNum - 1, sortName, direction);
		
		List<Movies> listMovies = page.getContent();
		
		model.addAttribute("currentPage", pageNum);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		
		model.addAttribute("sortName", sortName);
		model.addAttribute("direction", direction);
		model.addAttribute("invertDirection", direction.equals("asc") ? "desc" : "asc");
		
		model.addAttribute("listMovies", listMovies);
		
		return "index";
	}
	
	@RequestMapping("/new")
	public String showNewMovies(Model model) {
		Movies movies = new Movies();
		
		model.addAttribute("movies", movies);
		
		return "new_movies";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveMovies (@RequestParam("fileImage") MultipartFile multipartFile,
			@ModelAttribute("movies") Movies movies) {
		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		
		movies.setImage(fileName);
		
		moviesService.save(movies);
		
		String uploadDir = Constant.UPLOAD_FOLDER + "/" + movies.getMovieID();
		
		try {
			FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return "redirect:/";
	}
	
	@RequestMapping("/edit/{id}")
	public ModelAndView showEditMovies(@PathVariable(name = "id") Long movieID) {
		ModelAndView modelAndView = new ModelAndView("edit_movies");
		
		Movies movies = moviesService.get(movieID);
		
		modelAndView.addObject("movies", movies);
		
		return modelAndView;
	}
	
	@RequestMapping("/delete/{id}")
	public String deleteMovies (@PathVariable (name = "id") Long movieID) {
		moviesService.delete(movieID);
		return "redirect:/";
	}
}