package com.project.movielogin;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.project.movielogin.helpers.Constant;

public class MvcConfig implements WebMvcConfigurer {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		exposeDirectory(Constant.UPLOAD_FOLDER, registry);
		WebMvcConfigurer.super.addResourceHandlers(registry);
	}
	
	private void exposeDirectory(String dirName, ResourceHandlerRegistry registry) {
		Path uploadDir = Paths.get(dirName);
		
		String uploadPath = uploadDir.toFile().getAbsolutePath();
		
		System.out.println("exposeDirectory: "+uploadPath);
		
		registry.addResourceHandler("/" + dirName + "/**")
		.addResourceLocations("file: " + uploadPath + "/");
		
	}
}