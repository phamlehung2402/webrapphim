package com.project.movielogin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.project.movielogin.dao.MoviesRepository;
import com.project.movielogin.helpers.Constant;
import com.project.movielogin.models.Movies;

@Service
public class MoviesService {

	@Autowired
	private MoviesRepository repo;
	
	public Page<Movies> getMoviesPage (int pageNum){
		Pageable pageable = PageRequest.of(pageNum, Constant.PAGE_SIZE);
		
		return repo.findAll(pageable);
	}
	
	public Page<Movies> getMovesPage(int pageNum, String sortName, String direction){
		Pageable pageable = PageRequest.of(pageNum, Constant.PAGE_SIZE, direction.equals("asc")?Sort.by(sortName).ascending() : Sort.by(sortName).descending());
	
		return repo.findAll(pageable);
	}
	
	public List<Movies> getAllMovies(){
		return repo.findAll();
	}
	
	public void save(Movies movies) {
		repo.save(movies);
	}
	
	public Movies get(Long movieID) {
		return repo.findById(movieID).get();
	}
	
	public void delete(Long movieID) {
		repo.deleteById(movieID);
	}
}
