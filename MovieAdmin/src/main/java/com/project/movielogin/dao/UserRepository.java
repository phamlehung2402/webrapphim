Wpackage com.project.movielogin.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.project.movielogin.models.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	@Query("SELECT u From User u Where u.username = :username")
	public User getUserByUserName(@Param("username") String username);
}
