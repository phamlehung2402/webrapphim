package com.project.movielogin.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.movielogin.models.Movies;

public interface MoviesRepository extends JpaRepository<Movies, Long> {

}
