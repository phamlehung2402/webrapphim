drop database if exists QL_Phim;
create database QL_Phim;
use QL_Phim;

create table Movies(
	idPhim int not null auto_increment primary key,
    tenPhim nvarchar(200),
    ngonNgu nvarchar(50),
    doTuoi varchar(50),
    quocGia nvarchar(100),
    thoiLuong nvarchar(50), 
    doPhanGiai varchar(50),
    hinhAnh varchar(50), 
    trailer varchar(50),
    theLoai nvarchar(100)
);

create table KhachHang(
	idKh int not null primary key auto_increment,
    tenKH nvarchar(100),
    Sdt varchar(11),
    Email varchar(200),
    diaChi nvarchar(200),
    passwords varchar(100) not null,
    created_time TIMESTAMP NULL DEFAULT NULL,
    last_login TIMESTAMP NULL DEFAULT NULL,
    enabled TINYINT DEFAULT NULL,
    auth_provider VARCHAR(15) DEFAULT NULL
);

create table Quyen(
	idQuyen int not null auto_increment primary key,
    tenQuyen varchar(50) not null
);	

create table Users(
	idUser int not null auto_increment primary key,
    tenUser varchar(50),
    passwords varchar(100),
    enabled tinyint default 1
);
INSERT INTO users (tenUser, passwords) VALUES ('admin', '$2a$10$K3DvFPx/5BrF8MYJGIyBS.mBdcbHKxX6AAwz9rJ96azwMI1wU9rU.');
INSERT INTO users (tenUser, passwords) VALUES ('greena', '$2a$10$K3DvFPx/5BrF8MYJGIyBS.mBdcbHKxX6AAwz9rJ96azwMI1wU9rU.');
INSERT INTO users (tenUser, passwords) VALUES ('greenb', '$2a$10$K3DvFPx/5BrF8MYJGIyBS.mBdcbHKxX6AAwz9rJ96azwMI1wU9rU.');


create table User_Quyen(
	idUser int not null,
    idQuyen int not null,
    primary key  (idUser, idQuyen)
);
INSERT INTO user_quyen (idUser, idQuyen) VALUES ('1', '1');
INSERT INTO user_quyen (idUser, idQuyen) VALUES ('1', '2');
INSERT INTO user_quyen (idUser, idQuyen) VALUES ('1', '3');
INSERT INTO user_quyen (idUser, idQuyen) VALUES ('2', '2');
INSERT INTO user_quyen (idUser, idQuyen) VALUES ('2', '3');
INSERT INTO user_quyen (idUser, idQuyen) VALUES ('3', '3');

